extends KinematicBody2D
class_name Bee

const MAX_HEALTH := 100
const POLLEN_DISTANCE = 10
const QUANTITY_POLLEN = 5
const LIMIT_Y_TOP = 50
const LIMIT_Y_BOTTOM = 560

var Pollen = preload("res://UI/pollen/Pollen.tscn")

export (NodePath) var joystick_path;
var joystick_two;

var speed = 250.0
var position_player := Vector2.ZERO
const JOYSTICK_DEADZONE = 0.4

const SPEED_AUX = 250
var movement = 2.0

var array_pollen = []
var timer

var limit_A
var limit_B

var stress_manager : StressBarManager setget set_stress_bar_manager
var pollen_score_manager : Object

export var soundfx_hit : NodePath
export var soundfx_pollen_collected : NodePath
var is_on_waves_field : bool


func _init():
	timer = Timer.new()
	timer.wait_time = 0.63
	timer.autostart = true
	timer.connect("timeout", self, "create_pollen")
	GlobalVariables.player = self
	is_on_waves_field = false

	var resources = LevelResources.new()
	pollen_score_manager = resources.get_pollen_score_manager()
	stress_manager = resources.get_stress_bar_manager()


func create_pollen() :
	var new_pollen =  Pollen.instance()
	new_pollen.is_pollen_normal(false);
	new_pollen.visible = false;

	add_child(new_pollen)
	array_pollen.append(new_pollen)
	if array_pollen.size() >= QUANTITY_POLLEN:
		update_position_pollen()
		timer.stop()


func _ready():
	# warning-ignore:return_value_discarded
	UI_Events.connect("pollen_collected", self, "on_pollen_collected")
	add_child(timer)
	add_child(pollen_score_manager)
	add_child(stress_manager)
	
	joystick_two = get_node(joystick_path);
	joystick_two.connect("Joystick_Updated", self, "rotation_updated");
	$AnimationTree.active = true
	$CollisionShape.scale = Vector2(1, 1)

	set_default_limit()
	

func _physics_process(_delta):
	if is_on_waves_field:
		stress_aument()
	if (joystick_two.joystick_vector.length() > JOYSTICK_DEADZONE/2):
		# warning-ignore:return_value_discarded
		move_and_collide(-joystick_two.joystick_vector * speed * _delta * movement)

	$AnimationTree.set("parameters/orientation/current", 0)
	alling_pollen()
	get_limits()


func get_limits():
	global_position.y = clamp(global_position.y, LIMIT_Y_TOP, LIMIT_Y_BOTTOM)
	global_position.x = clamp(global_position.x, limit_A, limit_B)


func stress_aument():
	var stress = stress_manager.get_value()
	var stres_velocity = 10

	if (stress >= 80):
		speed = ((SPEED_AUX) * (70/stres_velocity))
		movement = -1
	elif (stress > 10):
		speed = ((SPEED_AUX) * (stress/stres_velocity))
		movement = 1
	else:
		speed = SPEED_AUX
		movement = 1


func rotation_updated():
	var pointer = joystick_two.joystick_vector
	if (pointer.length() > JOYSTICK_DEADZONE):
		rotation_degrees = rad2deg(global_position.angle_to_point(global_position + pointer))
		flip_h(in_range(abs(rotation_degrees), 90, 270))

			
func alling_pollen():
	if position_player != Vector2(0,0):
		update_position_pollen();


func update_position_pollen():
	var i = 0;
	for pollen in array_pollen :
		i = i + 1;
		if true :  # Solve
			pollen.set_center(i * (POLLEN_DISTANCE * Vector2.LEFT))
		else:
			pollen.set_center(i * (POLLEN_DISTANCE * Vector2.RIGHT))

		if QUANTITY_POLLEN == i:
			i = 0;


func in_range(number, mini, maxi) -> bool:
	return number >= mini && number <= maxi
	

func pick_pollen(pollen):
	pollen_score_manager.count(pollen.color)
	pollen.queue_free()
	

func on_pollen_collected(_ignored):
	var score_amount = pollen_score_manager.get_value()
	if score_amount < QUANTITY_POLLEN && score_amount < array_pollen.size():
		array_pollen[score_amount].visible = true


func is_alive() -> bool:
	if stress_manager != null:
		return stress_manager.get_value() < MAX_HEALTH
	else:
		return true


func static_scene(limit_a, limit_b):
	$Camera2D.current = false
	limit_A = limit_a
	limit_B = limit_b


func normal_scene():
	$Camera2D.current = true
	set_default_limit()


func set_default_limit():
	limit_A = 70
	limit_B = $Camera2D.limit_right


func reduce_stress_button(quantity_p:int, quantity_s:int):
	if stress_manager.value > 1 && pollen_score_manager.get_value() > 0 :
		stress_manager.rest(quantity_s)
		reduce_pollen(quantity_p)


func reduce_permit_boolean():
	return (stress_manager.value > 0 && pollen_score_manager.get_value() > 0)


func reduce_pollen(quantity_polen:int):
	var value = pollen_score_manager.get_value()
	if value < QUANTITY_POLLEN:
		array_pollen[value].visible = false
	pollen_score_manager.rest(quantity_polen)


func set_pollen_score_manager(manager, force_assign := false):
	if force_assign:
		pollen_score_manager = manager
	elif pollen_score_manager == null:
		pollen_score_manager = manager


func set_stress_bar_manager(manager):
	if stress_manager == null:
		stress_manager = manager


func flip_h(is_left : bool):
	$Sprite.flip_v = is_left
	$CollisionShape.scale.y = -1 if is_left else 1


func limit_space(limit_left, limit_right):
	$Camera2D.limit_right = limit_right
	limit_A = limit_left

