extends "res://Scripts/electro_waves/towers/level_controller.gd"

func run():
    if get_random_result():
        .generate_double_waves_v()
    else: 
        .generate_double_waves_h()