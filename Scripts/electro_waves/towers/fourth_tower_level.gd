extends "res://Scripts/electro_waves/towers/level_controller.gd"

func run():
    if get_random_result():
        .generate_double_waves_d(Vector2.UP)
        .generate_double_waves_d(Vector2.DOWN)
        .generate_double_waves_v()
        .generate_double_waves_h()
    else: 
        .single_wave()
        .single_wave()
