extends Area2D
class_name Projectile

var projectile_damage
var target # Bee

var velocity = Vector2(0, 0)
var can_launch = false
var speed: int = 100
var speed_increase : int

func _ready():
	$Sprite.playing = true
	add_to_group("electro_waves")


func _physics_process(delta):
	if can_launch:
		var direction = Vector2.RIGHT.rotated(rotation)
		global_position += speed * direction * delta


func set_target(reference_target):
	# Set target variable
	target = reference_target
	
	# Launch
	acquire_launch_position()
	acquire_target()
	can_launch = true
	

func _set_target(reference_target, default_position):
	# Set target variable
	target = reference_target
	# Launch
	set_launch_position(default_position)
	acquire_target()
	can_launch = true


func set_direction(reference_target, vector, default_position):
	target = reference_target
	set_launch_position(default_position)
	look_at(vector)
	can_launch = true	


# Just as the firm of the method
func acquire_launch_position():
	pass


func acquire_target():
	look_at(target.global_position)


func _on_ElectroWaves_body_entered(body):
	if body.name == "Bee":
		Game_Events.emit_signal("player_got_hit", projectile_damage)
		queue_free()


func _on_VisibilityEnabler2D_screen_exited():
	$Timer.stop()
	queue_free()


func _on_Timer_timeout():
	speed += speed_increase


func set_launch_position(position_tower):
	position = position_tower
