extends "res://Scripts/electro_waves/projectile.gd"

const DAMAGE = 2

func _init():
	speed += 80
	speed_increase = 100
	projectile_damage = 2

func _ready():
	$Sprite.set_animation("Normal")
	$Timer.wait_time = 0.1


func acquire_launch_position():
	# Get viewport boundaries
	var viewport_size = $VisibilityEnabler2D.get_viewport_rect().size
	var window_heigth = viewport_size.y
	var target_separation = 600
	
	var left_corner_up := Vector2(target.position.x - target_separation, 0)
	var left_corner_down := Vector2(target.position.x - target_separation, window_heigth)
	var right_corner_up := Vector2(target.position.x + target_separation, 0)
	var right_corner_down := Vector2(target.position.x + target_separation, window_heigth)

	var generation_corners = [left_corner_up, left_corner_down, right_corner_up, right_corner_down]
	position = generation_corners[randi() % generation_corners.size()]

