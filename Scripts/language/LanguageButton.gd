extends Node2D

var is_english = true

func _ready():
	$Start/Label.text = tr("change_languaje")

func _on_Start_pressed():
	is_english = not is_english
	determine_the_language()
	$Start/Label.text = tr("change_languaje")
	UI_Events.emit_signal("language_changed")

func determine_the_language():
	if is_english:
		TranslationServer.set_locale('en')
	else:
		TranslationServer.set_locale('es')
