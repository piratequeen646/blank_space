extends TextureButton

func _ready():
	pressed = GlobalVariables.is_sfx_muted


func _on_EfectsOff_pressed():
	UI_Events.emit_signal("sfx_music_off")
	GlobalVariables.is_sfx_muted = not GlobalVariables.is_sfx_muted
