extends Node
signal tree_game_paused


func pause_game():
	emit_signal("tree_game_paused")
	get_tree().paused = true


func resume_game():
	get_tree().paused = false


func quit_game():
	resume_game()
	GlobalVariables.reset()
	get_tree().reload_current_scene()
