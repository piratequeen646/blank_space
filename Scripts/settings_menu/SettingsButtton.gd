extends CanvasLayer

func _ready():
	UI_Events.connect("pop_up_active",self,"desactive_button")
	UI_Events.connect("pop_up_desactive",self,"active_button")

func _on_TouchScreenButton_pressed():
	UI_Events.emit_signal("game_paused", true)
	GlobalVariables.user_interface.hide()

func desactive_button():
	$ColorRect2/TouchScreenButton.set_block_signals(true)
	

func active_button():
	$ColorRect2/TouchScreenButton.set_block_signals(false)
