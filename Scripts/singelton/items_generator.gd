extends Node

func create_timer(wait_time):
	var current_timer = Timer.new()
	add_child(current_timer)
	current_timer.autostart = true
	current_timer.wait_time = wait_time
	return current_timer
