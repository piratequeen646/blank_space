extends Area2D
class_name Flower

var Pollen = preload("res://UI/pollen/Pollen.tscn")
var pollen_position = 60

var textures = [preload("res://Assets/animations/Flowers/flower_2.png"), 
				preload("res://Assets/animations/Flowers/flower_1.png")
]

var available_colors : Array = GlobalVariables.POLLEN_AVAILABLE_COLORS

var pollen_creation_timer
var is_hide = false
var amount_created_pollen = 0
const MAX_POLLEN_CREATION = 2 
const TO_GENERATION_WAIT_TIME = 2


func _init():
	pollen_creation_timer = Timer.new()
	pollen_creation_timer.autostart = false
	pollen_creation_timer.wait_time = TO_GENERATION_WAIT_TIME
	pollen_creation_timer.connect("timeout", self, "create_pollen")

func _ready():
	add_child(pollen_creation_timer)
	$Sprite.visible = false
	$Sprite.texture = textures[randi()%2]
	create_pollen()


func create_pollen():
	var pollen =  Pollen.instance()
	pollen.is_pollen_normal(true)
	pollen.color = available_colors[randi() % available_colors.size()]
	pollen.visible = true
	pollen.set_center(pollen_position * Vector2.UP)
	pollen.add_to_group("pollen")
	pollen.z_index = 1

	add_child(pollen)
	amount_created_pollen += 1
	pollen.connect("picked", self, "on_pollen_picked")
	pollen_creation_timer.stop()
	
	
func on_pollen_picked():
	if amount_created_pollen < MAX_POLLEN_CREATION:
		pollen_creation_timer.start()
		amount_created_pollen += 1
	else:
		fade_until_disappear()


func hide():
	sprite_collision_control(false)
	is_hide = true
	

func show():
	sprite_collision_control(true)
	is_hide = false


func _on_VisibilityNotifier2D_screen_exited():
	if (is_hide == false):
		sprite_collision_control(false)


func _on_VisibilityNotifier2D_screen_entered():
	if (is_hide == false):
		sprite_collision_control(true)


func sprite_collision_control(boolean:bool):
	$CollisionPolygon2D.disabled = boolean
	$Sprite.visible = boolean


func fade_until_disappear():
	$AnimationPlayer.play("opacity")


func _on_AnimationPlayer_animation_finished(anim_name:String):
	if anim_name == "opacity":
		queue_free()
