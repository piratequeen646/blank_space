extends Node2D
class_name BackgroundManager

var current : Background
var available_backgrounds = {"city": load("res://UI/background/City.tscn"),
							"hive": load("res://UI/background/Hive.tscn"),
							"urban_city": load("res://UI/background/Urban-City.tscn"),
							"park": load("res://UI/background/Park.tscn")}


func _init():
	# warning-ignore:return_value_discarded
	Game_Events.connect("level_started", self, "on_level_started")

func on_level_started(background : String):
	if current != null:
		current.stop_music()
		current.queue_free()

	current = available_backgrounds[background].instance()
	add_child(current)
	current.play_music()
