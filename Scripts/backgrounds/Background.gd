extends Node2D
class_name Background

var music_player : AudioStreamPlayer
var music_sound

func _ready():
	music_player = SingletonSound.make_player_bg(music_sound)
	add_child(music_player)
	
func play_music():
	music_player.play()


func stop_music():
	music_player.stop()
