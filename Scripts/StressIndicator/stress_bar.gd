extends CanvasLayer

const MIDDLE_POINT = 50
const MIN_DAMAGE = 2


func _ready():
	# warning-ignore:return_value_discarded
	UI_Events.connect("player_hit", self, "on_player_hit")
	# warning-ignore:return_value_discarded
	UI_Events.connect("play_animation", self, "play")
	change_language()
	$Bar.value = 0


func on_player_hit(score_value : int) -> void:
	$Bar.value = clamp(score_value, 0, 100)


func play(value: int):
	var animation = ""
	if value < 0: 
		animation = "Add_health" if $Bar.value < MIDDLE_POINT else "Add_health_plus"
	else:
		animation = "Add_stress" if value == MIN_DAMAGE else "Add_stress_plus"
	$AnimationPlayer.play(animation)


func set_max_value(max_value):
	$Bar.max_value = max_value

func change_language():
	$Bar/Name.text = tr("stress_bar")
