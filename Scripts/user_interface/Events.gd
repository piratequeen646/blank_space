extends Node

# warning-ignore:unused_signal
signal player_hit(score_value)
# warning-ignore:unused_signal
signal pollen_collected(amount)
# warning-ignore:unused_signal
signal play_animation(value)
# warning-ignore:unused_signal
signal full_collection()
# warning-ignore:unused_signal
signal bg_music_off()
# warning-ignore:unused_signal
signal sfx_music_off()
# warning-ignore:unused_signal
signal game_paused(flag)
# warning-ignore:unused_signal
signal kilometer_counter(value)
# warning-ignore:unused_signal
signal language_changed()

signal pop_up_active()

signal pop_up_desactive()
