extends CanvasLayer
class_name GUI

func _ready():
	GlobalVariables.user_interface = self

	var Settings_Area = CanvasLayer.new()
	Settings_Area.name = "Settings_Area"
	Settings_Area.add_child(load("res://Scenes/settings/SettingsButtton.tscn").instance())
	Settings_Area.add_child(load("res://Scenes/settings/SceneSettings.tscn").instance())
	.add_child(Settings_Area)


func add_child_first(node : Node):
	.add_child(node)
	move_child(node, 0)


func add_child(node : Node, _value := false):
	$Control.add_child(node)


func hide():
	for children in $Control.get_children():
		children.visible = false
	
	
func show():
	for children in $Control.get_children():
		children.visible = true
