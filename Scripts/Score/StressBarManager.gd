extends "res://Scripts/Score/ScoreManager.gd"
class_name StressBarManager

func init(sound_fx):
	.init(sound_fx)
	max_value = 100
# warning-ignore:return_value_discarded
	Game_Events.connect("player_got_hit", self, "sum")


func sum(amount: int):
	.sum(amount)
	UI_Events.emit_signal("play_animation", amount)


func rest(amount: int):
	.rest(amount)
	UI_Events.emit_signal("play_animation", amount * -1)


func render():
	UI_Events.emit_signal("player_hit", value)

