extends Node2D
class_name SequenceCollectionManager

var score_view : CounterGroup = load("res://UI/counter/GroupCounter.tscn").instance()
var sfx_player : PollenCollectionSounds = load("res://Scripts/sound_fx/PollenCollectionSounds.gd").new()
var pollen_score_manager : ScoreManager
var wanted_colors : Array
var aux = 0

func init(_wanted_colors, _pollen_score_manager : ScoreManager):
	pollen_score_manager = _pollen_score_manager
	wanted_colors = _wanted_colors


func _ready():
	GlobalVariables.user_interface.add_child(score_view)
	add_child(sfx_player.ready())
	score_view.measure_start_position(wanted_colors.size())
	for icon in wanted_colors:
		score_view.add_icon(icon)


func count(pollen_color: String):
	if wanted_colors[aux] == pollen_color:
		if aux < wanted_colors.size() - 1:
			process_sequence()
		else:
			mark_sequence_completed()
	else:
		reset()
		sfx_player.failed_sequence()


func process_sequence():
	sfx_player.play_next()
	score_view.activate_icon(aux)
	aux += 1


func mark_sequence_completed():
	sfx_player.play_next()
	pollen_score_manager.sum(wanted_colors.size())
	reset()


func reset():
	aux = 0
	score_view.deactivate_all()


func get_value():
	return pollen_score_manager.get_value()


func release():
	score_view.queue_free()
	queue_free()


func rest(amount : int):
	pollen_score_manager.rest(amount)