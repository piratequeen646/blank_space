extends Node
class_name ScoreManager

var value
var max_value
var sound_fx
var view_item


func init(sound):
	value = 0
	sound_fx = sound


func manage_view(view):
	view_item = view
	view_item.set_max_value(max_value)
	render()


func _ready():
	add_child(sound_fx)


func sum(new_value: int):
	var tmp = new_value + value
	if tmp < max_value:
		self.value += new_value
	else:
		value = max_value

	render()
	sound_fx.play()


func rest(new_value: int):
	if self.value > new_value:
		self.value -= new_value
	else:
		self.value = 0
	render()


func get_value() -> int:
	return value


# Method firm
func render():
	push_error(str("Cannot render empty value: ", value))


