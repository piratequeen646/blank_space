extends "res://Scripts/Score/ScoreManager.gd"
class_name PollenScoreManager

const MAX_SCORE = 25


func init(sound_fx):
	.init(sound_fx)
	max_value = MAX_SCORE
	value = 0
	# warning-ignore:return_value_discarded
	Game_Events.connect("level_finished", self, "save_data")


func count(_pollen_color: String):
	sum()


func sum(amount := 1):
	.sum(amount)


func render():
	if value >= 0:
		UI_Events.emit_signal("pollen_collected", value)


func save_data():
	GlobalVariables.picked_pollen = value


func restore():
	value = GlobalVariables.picked_pollen
