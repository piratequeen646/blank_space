extends Node

var player setget , get_player
var user_interface setget , get_user_interface
var scene_switcher setget , get_scene_switcher

var POLLEN_AVAILABLE_COLORS = Colors.colors.keys().slice(2, 4)

# Game level data
var game_stage_level = 0
var picked_pollen := 0

var is_bg_muted = false
var is_sfx_muted = false


func get_player():
	if player == null:
		push_error("player not loaded")

	return player


func get_user_interface():
	if user_interface == null:
		push_error("User interface not loaded")

	return user_interface


func get_scene_switcher():
	if scene_switcher == null:
		push_error("Scene switcher not loaded")

	return scene_switcher

func reset():
	game_stage_level = 0
	picked_pollen = 0
