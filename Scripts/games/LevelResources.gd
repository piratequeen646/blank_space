extends Object
class_name LevelResources

var PollenScore = load("res://UI/counter/Counter.tscn")
var StressBar = load("res://UI/stress_indicator/Stress_indicator.tscn")
var FlowersGenerator = load("res://UI/pollen/contend_flower.tscn")
var WavesGeneratorScript = load("res://Scripts/electro_waves/projectile_generator.gd")

var CounterExtra = load("res://UI/counter/Extra_Counter.tscn")
var CounterDefaultScript = load("res://Scripts/Counter/Extra_Counter.gd")

var PollenScoreManagerScript = load("res://Scripts/Score/PollenScoreManager.gd")
var StressBarManagerScript = load("res://Scripts/Score/StressBarManager.gd")

var player_got_hit_sound = SingletonSound.make_player_sound_fx(SingletonSound.PLAYER_GOT_HIT)
var pollen_collected_sound = SingletonSound.make_player_sound_fx(SingletonSound.POLLEN_COLLECTED, -20)

func on_view(scene_path):
	var item = scene_path.instance()
	GlobalVariables.user_interface.add_child(item)
	return item


func get_pollen_score():
	return on_view(PollenScore)


func get_stress_bar():
	return on_view(StressBar)


func get_flowers_generator(min_value, max_value, distance_between_flowers):
	var generator = FlowersGenerator.instance()
	generator.set_range_in_x(min_value, max_value)
	generator.set_distance_between_flowers(distance_between_flowers)
	return generator


func get_waves_generator():
	return WavesGeneratorScript.new()


func get_pollen_score_manager():
	var manager = PollenScoreManagerScript.new()
	manager.init(pollen_collected_sound)
	return manager


func get_stress_bar_manager():
	var manager = StressBarManagerScript.new()
	manager.init(player_got_hit_sound)
	return manager


func get_extra_counter(limit:int, signal_name:String, constant:String, script:Script = CounterDefaultScript):
	var counter = on_view(CounterExtra)
	counter.set_script(script)
	counter.set_constant_label(constant)
	counter.set_max_value(limit)
	counter._connect_to(signal_name)
	counter.update_text()
	return counter
