extends Node

# warning-ignore:unused_signal
signal level_started(bg_name)
# warning-ignore:unused_signal
signal level_finished()
# warning-ignore:unused_signal
signal player_got_hit(damage)
