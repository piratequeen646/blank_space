extends "res://Scripts/games/Level.gd"

var timer = load("res://UI/counter/Timer.tscn").instance()

const LEVEL_DURATION = 30
const MINIMAL_AMOUNT_POLLEN = 10

var flowers_generator
var counter_extra

func _init():
	level_condition = str(tr("condition_level1_part1"), MINIMAL_AMOUNT_POLLEN, tr("condition_level1_part2"), LEVEL_DURATION, "s?")
	level_bg = "hive"


func _ready() -> void:
	timer.connect("time_is_up", self, "finish")
	add_child(timer)
	pollen_score_view = level_resources.get_pollen_score()
	flowers_generator = level_resources.get_flowers_generator(325,625,700)
	add_child(flowers_generator)
	counter_extra = level_resources.get_extra_counter(MINIMAL_AMOUNT_POLLEN, "pollen_collected", "/" + String(MINIMAL_AMOUNT_POLLEN))


func start():
	.start()
	.check_player_score_manager()
	timer.start(LEVEL_DURATION)


func finish():
	if player.pollen_score_manager.value >= MINIMAL_AMOUNT_POLLEN:
		.remove_node(timer)
		.remove_node(pollen_score_view)
		.remove_node(flowers_generator)
		.remove_node(counter_extra)
		.finish()
	else:
		game_over()
