extends "res://Scripts/games/Level.gd"
var alert_waves = preload("res://UI/hazard_alert/hazard_alert.tscn").instance()
var stress_button = load("res://UI/stress_button/stress_button.tscn").instance()
var CounterKm :Script = preload("res://Scripts/Counter/Kilometer_counter.gd")
var level_progress_bar = load("res://UI/big_load_bar/LoadBar.tscn").instance()

const PLAYER_REQUIRED_DISPLACEMENT = 30000
const CONVERSION_UNIT = 10000

var increseament = 1
var start_player_pos
var waves_generator : Generator
var counter_extra
var limit_km = PLAYER_REQUIRED_DISPLACEMENT / CONVERSION_UNIT


func _init():
	._init()
	start_player_pos = player.position.x
	level_bg = "city"
	level_condition = tr("condition_level2")

	waves_generator = level_resources.get_waves_generator()
	level_progress_bar.init(PLAYER_REQUIRED_DISPLACEMENT)


func _ready():
	add_child(waves_generator)
	waves_generator.set_can_repeat(true)
	waves_generator.set_cooldown(2)

	alert_waves.show_alert()
	add_child(alert_waves)
	stress_button._ready()
	add_child(stress_button)

	pollen_score_view = level_resources.get_pollen_score()
	check_player_score_manager()
	stress_bar_view = level_resources.get_stress_bar()
	check_player_stress_bar_manager()

	counter_extra = level_resources.get_extra_counter(limit_km, "kilometer_counter" , " km ", CounterKm)
	GlobalVariables.user_interface.add_child(level_progress_bar)


func _process(_delta):
	if is_instance_valid(level_progress_bar):
		var position_x = player.position.x
		level_progress_bar.update_bar(position_x)
		UI_Events.emit_signal("kilometer_counter", stepify(position_x / CONVERSION_UNIT, 0.01))

		if position_x >= PLAYER_REQUIRED_DISPLACEMENT:
			finish()


func start():
	waves_generator.start()
	.start()


func finish():
	waves_generator.stop()
	.remove_node(pollen_score_view)
	.remove_node(stress_bar_view)
	.remove_node(alert_waves)
	.remove_node(level_progress_bar)
	.remove_node(counter_extra)
	.remove_node(stress_button)
	.finish()

