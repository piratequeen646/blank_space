extends CanvasLayer

var time_limit = 5
const TIME_WAIT_ANIMATION = 1

var timer: Timer
var is_pop_up

func init(_timer_limit:int):
	time_limit = _timer_limit

func start(message:String):
	is_pop_up = message != tr("stress_bitton")
	if is_pop_up:
		UI_Events.emit_signal("pop_up_active")
	timer = ItemsGenerator.create_timer(time_limit)
# warning-ignore:return_value_discarded
	timer.connect("timeout", self, "close")
	timer.start()
	$PopupDialog.visible = true
	$AnimationPlayer.play("appear")
	change_language(message)
	$PopupDialog/ObjectiveMessage.text = message


func _on_Button_pressed():
	close()


func close():
	UI_Events.emit_signal("pop_up_desactive")
	$AnimationPlayer.play_backwards("appear")
	timer.disconnect("timeout", self, "close")
	timer.wait_time = TIME_WAIT_ANIMATION
	# warning-ignore:return_value_discarded
	timer.connect("timeout", self, "clear")


func clear():
	$PopupDialog.visible = false
	$PopupDialog.queue_free()
	timer.stop()
	timer.queue_free()

func change_language(message):
	if message != tr("stress_button"):
		$PopupDialog/Title.text = tr("objetive_title")
