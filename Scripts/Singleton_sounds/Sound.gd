extends AudioStreamPlayer
class_name Sound

var default_value : int
var is_on_loop : bool
var has_muted : bool
var position : float

func init(file_sound, signal_name : String, volume, loop = false):
	UI_Events.connect(signal_name, self, "mute")
	
	stream = file_sound
	stream.loop = loop
	volume_db = volume
	is_on_loop = loop
	default_value = volume


func mute():
	if not has_muted:
		volume_db = -100
		position = get_playback_position()
		stop()
	else:
		volume_db = default_value
		if is_on_loop:
			play(position)
	has_muted = not has_muted
