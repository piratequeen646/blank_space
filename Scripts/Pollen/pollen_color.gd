extends Node2D
class_name ColorPollen

var color_pollen:Color
var color_light:Color

func _init(color_a:Color, color_b:Color):
	color_pollen = color_a
	color_light = color_b


func apply_effects(sprite : Sprite, light : Sprite) -> void:
	light.modulate = color_light
	sprite.modulate = color_pollen
