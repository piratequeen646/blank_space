extends Node2D
var pollen_color = load("res://Scripts/Pollen/pollen_color.gd")

var colors = {
	"YELLOW" : ColorPollen.new("#ec9f2e","#c5751c"),
	"SKY_BLUE" : ColorPollen.new("#36b6f5","#1c92c5"),
	"LILAC" : ColorPollen.new("#d872ed","#76158b"),
	"RED" : ColorPollen.new("#cc1212","#dc0000"),
	"GREEN" : ColorPollen.new("#3ba347","#09400c"),
	"WHITE" : ColorPollen.new("#ffffff","#ffffff"),
}


func get_color(key: String):
	return colors[key]
