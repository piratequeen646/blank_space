extends Area2D
class_name Pollen

const INIT_DISTANCE = -90;
const DISTANCE_PER_CYCLE = INIT_DISTANCE + 30;
const ROTATE_SPEED = 2;
const RADIUS_NORMAL = 10;
const RADIUS_BEE = 5;
const SCALE_ON_BEE = Vector2(0.07, 0.07)

var curren_radius = RADIUS_NORMAL;
var velocity = 40;
var is_up = true;
var _angle = 0;
var _center = Vector2(0, 0);
var is_hide = false
var is_in_bee = false

signal picked(pollen)

var color = "YELLOW"


func init(_color : String):
	color = _color


func _ready():
	var color_configuration = Colors.get_color(color)
	color_configuration.apply_effects($Sprite, $Light)


func _physics_process(delta):
	move_in_circle(delta, _center);
	


func translate(direction):
	position += direction; 


func move_in_circle(delta, center):
	_angle += ROTATE_SPEED * delta;
	var offset = Vector2(sin(_angle), cos(_angle)) * curren_radius;
	var pos = center - offset;
	position = pos;


func is_pollen_normal(it_is):
	if it_is:
		curren_radius = RADIUS_NORMAL;
		$CollisionShape2D.disabled = false;
	else:
		curren_radius = RADIUS_BEE;
		$CollisionShape2D.disabled = true;
		$Light.visible = false;
		$Sprite.scale = SCALE_ON_BEE
	is_in_bee = $CollisionShape2D.disabled


func set_center(center):
	_center = center;
	

func set_position(current_position):
	position = current_position;


func _on_Node2D_body_entered(body):
	var bee = GlobalVariables.get_player()
	if body == bee:
		emit_signal("picked")
		bee.pick_pollen(self)


func hide():
	if (is_in_bee == false):
		deny_visibility_for_performance(false)
		is_hide = true
	

func show():
	if (is_in_bee == false):
		deny_visibility_for_performance(true)
		is_hide = false


func _on_VisibilityNotifierPollen_screen_entered():
	if (is_in_bee == false && is_hide == false):
		deny_visibility_for_performance(true)


func _on_VisibilityNotifierPollen_screen_exited():
	if (is_in_bee == false && is_hide == false):
		deny_visibility_for_performance(false)


func deny_visibility_for_performance(performance:bool):
	$CollisionShape2D.disabled = !performance
	$Sprite.visible = performance
	$Light.visible = performance


func set_color(key: String):
	var color_configuration = Colors.get_color(key)
	color_configuration.apply_effects($Sprite, $Light)
