extends "res://Scripts/Counter/Extra_Counter.gd"


func count(value):
	if value < max_value:
		update_text(String(value))
	elif value >= max_value:
		update_text(String(max_value))
		complete()
