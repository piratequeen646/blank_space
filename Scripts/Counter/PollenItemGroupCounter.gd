extends Sprite

const LOW_OPACITY := 0.4
const NORMAL_OPACITY := 1

func init(_color_name : String, _position):
	var color_configurator = Colors.get_color(_color_name)
	color_configurator.apply_effects(self, $Light)
	visible = true
	position = _position
	low_opacity()


func low_opacity():
	modulate.a = LOW_OPACITY
	
func restore_opacity():
	modulate.a = NORMAL_OPACITY
