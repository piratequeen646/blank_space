extends CanvasLayer

var limit_pollen = 0
var counter_pollen = 0

func set_max_value(max_value):
	limit_pollen = max_value
	$limit.text = String(limit_pollen)
	

func _ready():
	$init.text = String(counter_pollen)
	$limit.text = String(limit_pollen)
	# warning-ignore:return_value_discarded
	UI_Events.connect("pollen_collected", self, "on_pollen_collected")
	$Name.text = tr("pollen_counter_label")


func update():
	$init.text = String(counter_pollen)


func set_init(init):
	counter_pollen = init
	_ready()


func on_pollen_collected(amount) -> void:
	counter_pollen = amount
	update()
