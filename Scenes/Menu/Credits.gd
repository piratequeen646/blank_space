extends CanvasLayer
const WAIT_TIME = 1
const CREDITS_DURATION = 59

func _ready():
	$ColorRect/Panel/Titles.text = tr("credits_text_titles")
	$ColorRect/Panel/Subtitle.text = tr("credits_text_subtitles")
	$ColorRect/Panel/Label.text = tr("credits_text_content")
	$Skip/Label.text = tr("credits_skip")
	$Timer.one_shot = true
	$Timer.wait_time = CREDITS_DURATION
	$Timer.connect("timeout", self, "finish")
	$Timer.start()

func _on_Skip_button_up():
	GlobalVariables.scene_switcher.change_scene("res://Scenes/Menu/Menu.tscn")
	
func finish():
	GlobalVariables.scene_switcher.change_scene("res://Scenes/Menu/Menu.tscn")
