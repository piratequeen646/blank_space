extends CanvasLayer
var music_player

func _init():
	var menu_music = load("res://Assets/audio/in_game_music/Eurynome.ogg")
	music_player = SingletonSound.make_player_bg(menu_music) as Sound


func _ready():
	add_child(music_player)
	music_player.play()
	$Bee/Sprite.flip_h = true
	change_language()
	# warning-ignore:return_value_discarded
	UI_Events.connect("language_changed",self,"change_language")


func _on_Credits_button_button_down():
	# warning-ignore:return_value_discarded
	GlobalVariables.scene_switcher.change_scene("res://Scenes/Menu/Credits.tscn")


func _on_Start_button_down():
	music_player.stop()
	# warning-ignore:return_value_discarded
	GlobalVariables.scene_switcher.change_scene("res://Game/GameCore.tscn")


func _on_Exit_button_down():
	get_tree().quit()


func change_language():
	$CanvasModulate/Label.text = tr("menu_title")
	$CanvasModulate/Start/Label.text = tr("menu_start_label")
	$CanvasModulate/Exit/Label.text = tr("menu_exit_label")
	$CanvasModulate/CreditsButton/Label.text = tr("menu_credits_label")
