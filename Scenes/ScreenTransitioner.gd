extends CanvasLayer

var level_message = {
	"hive" : [tr("label_transitioner_hive1"), tr("label_transitioner_hive2")], 
	"city" : [tr("label_transitioner_city1"), tr("label_transitioner_city2")], 
	"park" : [tr("label_transitioner_park1"), tr("label_transitioner_park2")], 
	"urban_city" : [tr("label_transitioner_urban_city1"), tr("label_transitioner_urban_city2")]
}
signal transitioned
var value = 0
var percent = "%"


func _ready():
	_elements_visible(false)


func transition(level_name:String):
	GameControl.pause_game()
	$Timer.start()
	_elements_visible(true)
	var quote = label_by_level(level_name).split("-")
	$Quote/Label.text = quote[0]
	$Quote/author.text = quote[1]
	$AnimationPlayer.play("fade_to_black")
	$Label2.set_text(str(value, percent))
	

func label_by_level(level_name:String):
	var messages = level_message[level_name]
	return messages[randi() % messages.size()]


func _on_AnimationPlayer_animation_finished(anim_name):
	if anim_name == "fade_to_black":
		$AnimationPlayer.play("fade_to_normal")
		GameControl.resume_game()
		_elements_visible(false)
		$Timer.stop()
		value = 0
		emit_signal("transitioned")
		


func _on_Timer_timeout():
	value += 2
	$Label2.set_text(str(value, percent))


func _elements_visible(visible:bool):
	$Label2.visible = visible
	$Quote.visible = visible
	$AnimatedSprite.visible = visible
