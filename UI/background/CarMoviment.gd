extends ParallaxLayer


export var CAR_SPEED = +90

func _process(delta):
	self.motion_offset.x += CAR_SPEED * delta
